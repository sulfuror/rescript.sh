## About rescript
`rescript` is a bash shell wrapper for [Restic](https://restic.net/) that makes easy to configure repositories and work with them. The script was made for GNU/Linux systems but it may also work on MacOS and FreeBSD.

## Index

1. [Mac and BSD Systems](https://gitlab.com/sulfuror/rescript.sh/-/wikis/mac-and-bsd-systems)
2. [Installation](https://gitlab.com/sulfuror/rescript.sh/-/wikis/installation)
3. [Usage](https://gitlab.com/sulfuror/rescript.sh/-/wikis/usage)
4. [Configuration Files](https://gitlab.com/sulfuror/rescript.sh/-/wikis/configuration-files)
5. [Commands and Options](https://gitlab.com/sulfuror/rescript.sh/-/wikis/commands-and-options)
6. [Security](https://gitlab.com/sulfuror/rescript.sh/-/wikis/security)
7. [Cron Jobs](https://gitlab.com/sulfuror/rescript.sh/-/wikis/cron-jobs)
8. [Some Things Worth to Mention](https://gitlab.com/sulfuror/rescript.sh/-/wikis/some-things-worth-to-mention)
9. [Workarounds](https://gitlab.com/sulfuror/rescript.sh/-/wikis/workarounds)
10. [Remove rescript](https://gitlab.com/sulfuror/rescript.sh/-/wikis/remove-rescript)

| **DISCLAIMER / USE AT YOUR OWN RISK** 
| --------------
|**_Use at your own risk_**. I'm not a developer, programmer or anything related; I'm just a regular user sharing my basic knowledge. I created this for my personal use but decided to share it because when I was looking for something like this, I did not find something that could fulfill my expectations. I know maybe there are some things in my script that can be done in a different way, better or even more easily but unfortunately I don't have enough knowledge. You're more than welcome to get in touch if you want to contribute, fix, add something, etc.

## Having problems?
If you have any problem with the script you can [open an issue](https://gitlab.com/sulfuror/rescript.sh/issues) or reach out so it can be fixed. If you have any problem using restic check out the [restic forum](https://forum.restic.net/); maybe you can find answers or submit a question about your problem. I'm no affiliated with the **restic** team in any way.

## Based on:
This script based on an example of a Restic Script found in
the following link: https://pastebin.com/ydN9fJ4H.

The original script was made for Borg and you can find it at this site:
https://blog.andrewkeech.com/posts/170718_borg.html

My intention is not to steal someone elses work so that's why I need to disclose the original source. I found all sources in this Reddit thread:
https://www.reddit.com/r/ScriptSwap/comments/7v7vby/restic_backup_script/

**Note**: the latest script is barely something like the original mentioned above. If you see the first versions it really was similar.
